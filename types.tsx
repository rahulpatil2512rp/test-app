export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Records: undefined;
};

export type Data = {

  Id: number,
  title: string,
  shortDescription: string,
  collectedValue: number,
  totalValue:number,
  startDate: string,
  endDate:string,
  mainImageURL: string

};